package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-gonic/gin"
	docs "gitlab.com/DaveBlack/server-gin-df/docs"
)

var appVersion, commit, buildTime string
var versions Versions
var censors = make(map[string]bool)
var mu sync.Mutex
var statusError int

// @BasePath /api/v1
// PingExample godoc
// @Summary ping example
// @Schemes
// @Description do ping
// @Tags example
// @Accept json
// @Produce json
// @Success 200 {string} Helloworld
// @Router /example/helloworld [get]

func main() {

	versions.Version = appVersion
	versions.Commit = commit
	versions.BuildTime = buildTime

	fmt.Println(versions.BuildTime, versions.Commit, versions.Version)
	router := gin.Default()

	docs.SwaggerInfo.BasePath = "/api/v1"
	v1 := router.Group("/api/v1")
	{
		eg := v1.Group("/example")
		{
			eg.GET("/version", version)
			eg.GET("/authors", getAuthors)
			eg.GET("/books", getBooks)
			eg.POST("/censors", enpSetCensors)
		}
	}
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	router.Run("localhost:8080")

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	srv := &http.Server{
		Addr: ":8080",
	}

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Listen for the interrupt signal.
	<-ctx.Done()

	// Restore default behavior on the interrupt signal and notify user of shutdown.
	stop()
	log.Println("shutting down gracefully, press Ctrl+C again to force")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 80*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
	}

	log.Println("Server exiting")

	//log.Fatal(http.ListenAndServe(":8080", nil))

}

// @BasePath /api/v1

// PingExample godoc
// @Summary Shows an version
// @Schemes
// @Description Shows an version, commit hash, build time
// @Tags version
// @Accept json
// @Produce json
// @Success 200 {object} Versions "desc"
// @Router /version [get]
func version(c *gin.Context) {

	c.IndentedJSON(http.StatusOK, Versions{Version: versions.Version, Commit: versions.Commit, BuildTime: versions.BuildTime})
}

func getAuthors(c *gin.Context) {

	// curl http://localhost:8080/api/v1/authors?book=OL7353617M

	bookAuthors := Book{}
	name := Name{}
	id := c.Query("book")
	fmt.Println("book ", id)

	url := "https://openlibrary.org/isbn/" + id + ".json"

	body, err := apiCall(url)
	if err != nil {
		log.Println(err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "failed api call"})
		return
	}

	err = json.Unmarshal(body, &bookAuthors)
	if err != nil {
		log.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "cannot create error"})

	}

	respA := make([]RespAuthor, len(bookAuthors.Authors))

	for index := range bookAuthors.Authors {
		url = "https://openlibrary.org" + bookAuthors.Authors[index].Key + ".json"
		body, err = apiCall(url)
		if err != nil {
			c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "error"})
			return
		}
		err := json.Unmarshal(body, &name)
		if err != nil {
			log.Println("error:", err)
			c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "cannot create error"})
		}

		bookAuthors.Authors[index].Key = strings.ReplaceAll(bookAuthors.Authors[index].Key, "/authors/", "")

		respA[index].Name = name.Name
		respA[index].Key = bookAuthors.Authors[index].Key
	}

	c.IndentedJSON(http.StatusOK, respA)
}

// @BasePath /api/v1

// PingExample godoc
// @Summary Shows an version
// @Schemes
// @Description Shows an version, commit hash, build time
// @Tags version
// @Accept json
// @Produce json
// @Success 200 {object} Versions "desc"
// @Router /books [get]
func getBooks(c *gin.Context) {

	// curl localhost:8080/api/v1/books?author=OL1394244A
	//bookAuthors := Book{}
	//name := Name{}
	authorBooks := Entries{}

	author := c.Query("author")

	if author == "" {
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "missing URL param book which has to contain isbn of book"})
		return
	}
	if isCensored(author) {
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "this author is censored and his/her books will not be shown"})
		return
	}

	url := "https://openlibrary.org/authors/" + author + "/works.json?limit=2"

	body, err := apiCall(url)
	if err != nil {
		log.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "cannot create error"})
		return
	}

	err = json.Unmarshal(body, &authorBooks)
	if err != nil {
		log.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "cannot create error2"})

	}

	respB := make([]RespBooks, len(authorBooks.Entries))

	for index := range authorBooks.Entries {

		respB[index].Name = authorBooks.Entries[index].Title
		respB[index].Key = authorBooks.Entries[index].Key
		respB[index].Revision = authorBooks.Entries[index].Revision
		respB[index].PublishDate = authorBooks.Entries[index].Created.Value
	}

	c.IndentedJSON(http.StatusOK, respB)

}

// @BasePath /api/v1

// PingExample godoc
// @Summary Censorship
// @Schemes
// @Description Censors some bad guy by his ID
// @Tags censors
// @Accept json
// @Produce json
// @Success 200 {string} nice
// @Router /censors [post]
func enpSetCensors(c *gin.Context) {

	// curl  -X POST localhost:8080/api/v1/books -H 'Content-type: application/json' -d '["OL1394244A"]'
	// curl  -X POST localhost:8080/api/v1/censors -H 'Content-type: application/json' -d '["OL1394244A"]'

	//censor := c.Query("censors")
	//fmt.Println(censor)
	reqCensors := []string{}

	err := c.BindJSON(&reqCensors)
	if err != nil {
		log.Print(err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "cannot bind json"})
		return
	}
	setCensors(reqCensors)
	//c.IndentedJSON(http.StatusOK, reqCensors)
	//c.IndentedJSON(http.StatusOK, nil)
	c.Status(http.StatusOK)

}

func setCensors(cens []string) {
	mu.Lock()
	for _, c := range cens {
		censors[c] = true
	}
	mu.Unlock()
}

func isCensored(author string) bool {
	mu.Lock()
	defer mu.Unlock()
	return censors[author]
}

func apiCall(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()

	// !!! nacitava cely []byte do pamate, lepsie bufio.NewReader()
	body, err := ioutil.ReadAll(resp.Body)

	return body, err
}

func sendError(w http.ResponseWriter, httpCode int, err error) {
	buf, locErr := json.Marshal(ErrorResponse{Error: err.Error()})
	if locErr != nil {
		log.Println("origin error", err)
		log.Println("json marshall error", locErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(httpCode)
	w.Write(buf)
}

// merge request try
//curl localhost:8080/api/v1/books?author=OL1394244A
/*
func accessLog() {
	GetIpAddress()


}*/

func GetIpAddress(w http.ResponseWriter, r *http.Request, statusError *int) string {
	ip := r.RemoteAddr
	rUrl := r.URL
	met := r.Method
	cd := *statusError
	now := time.Now()
	fmt.Println(ip, rUrl, cd, met, now)

	return ip
}

//func middlewareEndpoint(w http.ResponseWriter, r *http.Request) {}

//	403 forbidden if auth not working
//
// vytvorime funkciu ktora bude zabalovat funkciu
//func encapsulationHandler(endpoint func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {

//}

//func basicAuth(w http.ResponseWriter, req)

/// kill Term pid prikaz na ukoncenie procesu

/*
exit := make(chan os.Signal, 1)
signal.Notify(exit, os.Interrupt, syscall.SIGTERM)
*/
